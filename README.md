# Data-Analytics-with-PyTorch
PyTorch is an open source machine learning library based on the Torch library, used for applications such as computer vision and natural language processing, primarily developed by Facebook's AI Research lab. It is free and open-source software released under the Modified BSD license.
PyTorch is a Python package that provides two high-level features:

    1. Tensor computation (like NumPy) with strong GPU acceleration
    2. Deep neural networks built on a tape-based autograd system

**Installing on Linux**
PyTorch can be installed and used on various Linux distributions. Depending on your system and compute requirements, your experience with PyTorch on Linux may vary in terms of processing time. It is recommended, but not required, that your Linux system has an NVIDIA GPU in order to harness the full power of PyTorch’s CUDA support. First install Python and PIP.

~~~sudo apt install python~~~

While Python 3.x is installed by default on Linux, pip is not installed by default.

~~~sudo apt install python3-pip~~~

Then install with  pip.
pip install torch

**Pytorch version check**

~~~import torch~~~
~~~print(torch.__version__)~~~
